#!/usr/bin/python

import csv
import matplotlib.pyplot as plt
import numpy as np
import re

def plot(horiz, vert, colors, tit, xlab, ylab):
	plt.figure()
	for i in range(0, len(vert)):
		plt.plot(horiz, vert[i], color=colors[i])
	plt.title(tit)
	plt.ylabel(ylab)
	plt.xlabel(xlab)
	plt.show()


def plotFreqs(freqs, nTh, logPath, fileName):
	means = [[],[]]
	for i, f in enumerate(freqs):
		file = fileName + '_' + str(nTh) + '_'+ str(f) + '.txt'
		with open(logPath+file, 'rb') as csvfile:
			csvfile.readline()
			csvfile.readline()
			datalog = csv.reader(csvfile, delimiter=',', quotechar='|')
			test = 3
			virt = []
			for row in datalog:
				if test > 0:
					[ t, p, c, v, cp, vp ] = row

					if float(p) >= 2.0:  # all staurate power must be greater than 2.0
						if test == 3:
							test -= 1
							res = []
						res.append((float(p),float(c),float(v)))
					elif test < 3 and len(res) > 0:
						virt.append(res[1:-1])
						res = []
						test -= 1

		# Plot the result
		power = [i[0] for i in virt[0]]
		means[0].append(sum(power)/len(power))
		power = [i[0] for i in virt[1]]
		means[1].append(sum(power)/len(power))
		x = range(0, len(power))
		# plt.plot(x, power)
		# plt.title('Power - ' + str(f) + 'Hz')
		# plt.ylabel('Power[W]')
		# plt.xlabel('Time [s]')
		# plt.show()
	return means
	

def plotVolts(volts, nTh, logPath):
	means = []
	for i, v in enumerate(volts):
		file = 'res_' + str(v) + '_'+ str(nTh) + '.txt'
		content = open(logPath + file, "r")
		file_v = []
		for line in content:
			if re.match("volt=", line):
				_, v = line.split('=')
				file_v.append(float(v[:-2]))
		means.append(sum(file_v)/len(file_v))
	return means

colors = ['r', 'g', 'b', 'y', 'k']
name_v = 'res_virtual/'
name_f = 'res_fisico/'

# Freqs
frequencies = [700, 800, 900, 1000, 1100, 1200]
means_4_proc=plotFreqs(frequencies, 4, name_f+'freq/', 'freq')
means_1_proc=plotFreqs(frequencies, 1, name_f+'freq/1_thread_4_cores/', 'freq')
plot(frequencies, means_4_proc, ['b', 'r'], 'Power - Freq', 'Frequency [MHz]', 'Mean Power[W]')
plot(frequencies, means_1_proc, ['y', 'b'], 'Power - Freq', 'Frequency [MHz]', 'Mean Power[W]')

# Volts
# voltages = [-3, -1, 1, 3, 5]	
# means_v= plotVolts(voltages, 4, name_v + 'volt/')
# plot(voltages, [means_v], ['r'], 'Voltage', 'over_voltage', 'Voltage [V]')
# v_teor = [125, 175, 225, 275]
# means_p_voltage=plotFreqs(v_teor, 4, name_f+'volt/', 'volt')
# plot(v_teor, means_p_voltage, ['b', 'r'], 'Power - Voltage', 'Voltages [mV]', 'Mean Power[W]')
