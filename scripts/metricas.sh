#!/bin/bash

echo "PID de las metricas: $$"
while [ 1 ]
do 
	echo "Governor:   $(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor)"
	echo "Freq [MHz]: $(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq)"
	echo "Temp [ºC]: $(cat /sys/class/thermal/thermal_zone0/temp)"
	/opt/vc/bin/vcgencmd measure_volts core
	sleep 1
done
