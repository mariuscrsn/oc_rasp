
GOVERNOR="ondemand"
WORKING_DIR=$(pwd)
CONF_FILE="../my_config.txt"
ORG_FILE="../config.txt_org"

cp /boot/config.txt "${WORKING_DIR}/../config.txt_prev"
echo "Backup realizado ..."

echo "============================\nOpciones de OC configuradas:\n"
cat  "${WORKING_DIR}/${CONF_FILE}" "${WORKING_DIR}/${ORG_FILE}"  > /boot/config.txt
cat "${WORKING_DIR}/${CONF_FILE}" | grep -v "#"
