#!/bin/bash

if [ $# -ne 2 ]
then
	echo "Uso: $0 <Nº primos> <Nº threads> "
	exit -1
fi
N_PRIM=$1
N_THREADS=$2
N_MUESTRAS=8
#FREQ=$(head -n 1 /boot/config.txt | cut -d'=' -f2)
#FOLDER="freq"
#FILE="../res_sw/${FOLDER}/freq_${FREQ}_${N_THREADS}_1cores.txt"

FOLDER="volt"
VOLT=$(grep over_voltage= /boot/config.txt | cut -d'=' -f2)
FILE="../res_sw/${FOLDER}/volt_${VOLT}_${N_THREADS}_4cores.txt"


touch ${FILE}
# Cambia a performarce el governor
echo "performance" | sudo tee /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
./metricas.sh > ${FILE} &
MET_PID=$!
echo "PID de las metricas: ${MET_PID}"

# Ejecuta sysbench y mide el tiempo

inicio=$(date)
start=$(date +%s)
sysbench --test=cpu --cpu-max-prime=${N_PRIM} --num-threads=${N_THREADS} run
end=$(date +%s)
fin=$(date)
echo "Prueba terminada, procesando métricas..."
kill ${MET_PID}

# Analisis de resultados si hay mas de N_MUESTRAS
temp=$(cat ${FILE} | grep Temp | cut -d':' -f2 | tr -d " " | sort)
volt=$(cat ${FILE} | grep volt | cut -d'V' -f1 | cut -d'=' -f2 | tr "\n" "+")
num=$(cat ${FILE} | grep Temp | wc -l)
echo ${num} 

if [ ${num} -lt ${N_MUESTRAS} ]
then
	echo "ERROR: Se necesitan mas de ${N_MUESTRAS} muestras"
 	exit -1
fi


volt=$(echo "(${volt::-1})/${num}" | bc -l)
temp_mediana=$(echo ${temp} | tr -s " " "\n" | tail -n +$(( ${num}/2 + 1 )) | head -n 1)
temp_media=$(echo ${temp} | tr -s " " "\n" | tail -n +$(( (${num}-${N_MUESTRAS})/2+1 )) | head -n ${N_MUESTRAS} | tr -s "\n" "+")
temp_media=$(echo "(${temp_media::-1})/${N_MUESTRAS}" | bc -l)

# Restaura el estado anterior y muestra resultados
echo "ondemand" | sudo tee /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo -e "=====Metricas=====\nN.medidas: ${num}\n$(head -n 3 ${FILE} | grep Freq)"
echo "Temp media=${temp_media} ºC"
echo "Temp mediana = ${temp_mediana} ºC"
echo "Voltaje medio=${volt} V"
echo "ondemand" | sudo tee /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo "Tiempo: $((end-start))s"
echo "Tiempo inicial: ${inicio}, Tiempo final: ${fin}" >> ${FILE} 
