# Configuración básica para hacer OC en raspberry
Dirección ip raspberry; pi@169.254.125.204, pi

- Fichero /etc/init.d/raspi-config &rarr; activa modo `ondemand` en caso de que no se mantenga pulsada la tecla SHIFT en el start up
- Como configurar OC y hacer script para controlar la carga &rarr; https://www.jackenhack.com/raspberry-pi-3-overclocking/
- Cambiar modo ondemand &rarr; https://raspberrypi.stackexchange.com/questions/9034/how-to-change-the-default-governor 


**V: 0.8 - 1.4**    
**freq: 600 - 14500**


SO for raspberry:
- Ubuntu: https://www.ubuntu.com/download/iot/raspberry-pi-2-3
- Debian: https://wiki.debian.org/RaspberryPi3
- Arch linux: https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3


### Freq:
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq
/opt/vc/bin/vcgencmd measure_clock arm


### Voltaje: 
```
vcgencmd measure_volts core
```
cat /sys/class/thermal/thermal_zone0/temp_freq

AVS (Adaptative voltage scalling) --> por defecto en la raspberry está a 1.2V
El valor de over_voltage varía entre -16 y 8



### Temperatura:
```
/opt/vc/bin/vcgencmd measure_temp
```


###Instrucciones para realizar OC
- Instalar sysbench 
- Instalar bc
- Ejecutar ./configurar_OC.sh con el parámetro deseado de arm_freq
- Reboot
- Ejecutar ./ejecutar_exp.sh


#LABORATORIO DE EMPOTRADOS
###Medición Voltaje con los TEST_PADS
- Mirar layout_testpads.pdf y medir en el PP58 para encontrar el vcore --> con Vcc = 1.200 --> 1.204; Vcc = 1.2813 --> 1.296

-Los chips más rápidos necesitan más voltaje pero consumen menos Pestatica 
- El BCM2837 alimenta el coreSMPS que genera el voltaje del core. La raspberry utiliza un Switch Mode Power Supply(Son más pequeños que los lineales y tienen una alta eficiencia pero suelen tener EMI, supongo que no proporcionan un voltaje tan continua). VBAT, señal que va del SODIMM connector al coreSMPS. 
- BAT conectado al pin 197-200 del Compute Module SODIMM.
- Rango especificado por el VBAT: -0.5V === 6.0V
- Rango de operacion VBAT: 2.5V === 5.0V + 5%
- Versión kernel: 4.14.79

### Deshabilitacion Cores
- taskset -c 0,1 prog   se puede asignar un proceso a una cpu solo
**Power Gating:** In a processor chip, certain areas of the chip will be idle and will be activated only for certain operations. But these areas(cmos) are still provided with power for biasing. The power gating limits this unnecessary power being wasted by shutting down power for that area and resuming whenever needed.
Power gating is used for reducing LEAKAGE POWER by switching off power supply to the non operational power domain of the chip during certain mode of operation. Header and footer switches, isolation cells and state retention flip flips (SRFFs) are used for implementing power gating.

**Clock Gating:** clock gating limits the clock from being given to every register or flops in the processor.In clock gating the gated areas will still be provided with bias power.
Clock gating is used for reducing DYNAMIC POWER by controlling switching activities on the clock path. Generally Gate or Latch or FF based clock gating cells are used for implementing clock gating.

### Governors

- Top comand output: cpu utilization: us(userland), sy(kernelspace), ni(procesos con nice cambiado), id(idle), wa(wait for IO), hi, si(sw, hw int), st(virtualizacion)
- El CPUFreq es16 un framework proporciona el código infraestructura y  la interfaz con el usuario. CPUFreq: core, scaling governors(algoritmo para estimar la CPU FyV) y scaling drivers(aportan al governors los P-estados disponibles o rangos y una interfaz para cambiar de estado, hay algunos drivers que implenetan sus governors).
- Cpufreq_policy{min, max, governor}. Tiene una lista de govs disponibles y una interfaz para obtener governores, poner algún otro como si fuera un driver. la carga la mide cada cierto tiempo comparando el tiempo de idle y ejecución.
- SO-DIM 197-200 son el vbat. DBGPWRDUP[CN:0] registro del core para apagarlo?

### EXPERIMENTACIÓN
 - res_hw &rarr; contiene las medidas tomadas con el multímetro
 - res_sw &rarr; medidas tomadas por software
 - Los ficheros almacenados en cada una de estas carpetas tienen la siguiente nomenclatura: [freq | volt]/[freq | volt]_[mV|mHz]_#threads.txt"
 - Los ficheros que toman medidas software tienen el siguiente formato:
 ```
Governor:   performance
Freq [MHz]: 800000
Temp [ºC]: 38090
volt=1.2750V
 ```
 Al final hay una linea con el tiempo de inicio y fin de cada prueba. 
 - Los ficheros que almacenan medidas tomadas con el PPA520 tienen el siguiente formato:
 ```
Tiempo 	     ,DC Watts, DC Current,DC Voltage,Peak Current,Peak Voltage
20:39:16.036,1.89970E0,-3.59610E-1,-5.28250E0,-5.35450E-1,-5.69360E0
 ```
 - En la hoja de calculo resultados hay unas tablas con el número de cores utilizados, la frecuencia y el número de primos calculados.
 

### REFERENCIAS
- Documentación oficial overclocking raspberry https://www.raspberrypi.org/documentation/configuration/config-txt/overclocking.md
- Introduction Turbo-Mode https://www.raspberrypi.org/blog/introducing-turbo-mode-up-to-50-more-performance-for-free/
- Uso de vcgencmd https://elinux.org/RPI_vcgencmd_usage
- https://haydenjames.io/raspberry-pi-3-overclock/
- https://eltechs.com/overclock-raspberry-pi-3/
Power gatting
- change the default governor on boot &rarr; https://www.raspberrypi.org/forums/viewtopic.php?t=114536
- Datasheet y schematics compute module raspberry &rarr; https://www.raspberrypi.org/documentation/hardware/computemodule/
- Especificaiones técnicas raspberry &rarr; https://www.terraelectronica.ru/pdf/show?pdf_file=%252Fds%252Fpdf%252FT%252FTechicRP3.pdf
- Foro de sudomod con alguno de los testpads &rarr; https://sudomod.com/forum/viewtopic.php?f=23&t=1491
- Experimento muy parecido al mio, hace OC extremo con nitrógeno. Además, alimenta la raspberry con una fuente de 1500W.  &rarr; https://xdevs.com/article/rpi3_oc/#voltmod
- Top command output - https://www.booleanworld.com/guide-linux-top-command/
- Power gatting raspberry - https://www.raspberrypi.org/forums/viewtopic.php?t=152692